# -- Questão 1 --
num1 = input('Insira o primeiro número: ')
num2 = input('Insira o segundo número: ')

if num1 > num2:
    print('O maior número é o primeiro: ', num1)
else:
    print('O maior número é o segundo: ', num2)

# -- Questão 2 --
porcentagem = float(input('Insira a porcentagem: '))

if porcentagem > 0:
    print('Houve um crescimento de produção de ', porcentagem, '%')
else:
    print('Houve um decrescimento de produção de ', porcentagem, '%')
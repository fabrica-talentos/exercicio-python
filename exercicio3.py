car_list = []
print('◊----- Bem-vindo a loja de carros -----◊')

while True:
    print('1. Cadastrar novo carro')
    print('2. Listar os carros')
    print('3. Comprar um carro')
    print('4. Sair do programa')

    choice = int(input('Insira a opção desejada: '))
    print('◊-----------------------◊')
    if choice == 1:
        print('Cadastrando novo carro')
        marca = input('Insira a marca do carro: ')
        modelo = input('Insira o modelo do carro: ')
        preco = int(input('Insira o preço: '))
        carro = {'marca': marca, 'modelo': modelo, 'preco': preco}
        car_list.append(carro)

    elif choice == 2:
        print('Mostrando todos os carros da loja')
        for carro in car_list:
            print(carro)

    elif choice == 3:
        modeloCarro = input('Insira o modelo do carro que deseja comprar: ')
        for carro in car_list:
            if carro['modelo'] == modeloCarro:
                car_list.remove(carro)
                print('Carro comprado com sucesso!')
                break

    elif choice == 4:
        break

    else:
        print('Opção inválida, tente novamente...')
    print('◊-----------------------◊')

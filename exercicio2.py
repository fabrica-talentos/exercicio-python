# -- Questão 1 --
print('--- Questão 1 ---')
num1 = int(input('Digite o primeiro número: '))
num2 = int(input('Digite o segundo número: '))

print(f'mostrando todos os números inteiros entre {num1} e {num2}: ')
for n in range(num1+1, num2):
    print(n)

# -- Questão 2 --
print('--- Questão 2 ---')
tabuada = int(input('Digite um número: '))
print('--------◊--------')
for n in range(0, 11):
    print(f'{n} x {tabuada} = {n * tabuada}')
print('--------◊--------')

"""
print('Olá, Mundo!')
print(10)

nome = input('Digite o nome do estudante: ')
ano_entrada = int(input('Digite o ano de ingresso do estudante: '))
nota = float(input('Digite a nota do estudante: '))

if nota >= 60:
    print('Passou de ano!')
elif 30 <= nota < 60:
    print('Ta de NP3')
else:
    print('Fazer de novo semestre que vem :(')

t1 = t2 = True
f1 = f2 = False

if t1 and f2:
    print('Expressão verdadeira')
else:
    print('Expressão falsa')

if t1 or f2:
    print('Expressão verdadeira')
else:
    print('Expressão falsa')

if not f1:
    print('Expressão verdadeira')
else:
    print('Expressão falsa')

lista = 'Leandro, Guilherme, João, Pedro, Ana'
nome_1 = 'Leandro'
nome_2 = 'José'

if nome_1 in lista:
    print(f'{nome_1} está na lista')
else:
    print(f'{nome_1} não está na lista')

cont = 1
while cont <= 3:
    nota_1 = float(input("Insira a primeira nota: "))
    nota_2 = float(input("Insira a segunda nota: "))
    print(f'A média do aluno foi: {(nota_1 + nota_2) / 2}')
    cont += 1

for cont in range(1, 11):
    print(cont)

for count in range(1, 4):
    nota_1 = float(input("Insira a primeira nota: "))
    nota_2 = float(input("Insira a segunda nota: "))
    print(f'A média do aluno foi: {(nota_1 + nota_2) / 2}')

lista = [1,2,3,4,5,6,7,8,9,10]

for elemento in lista:
    print(elemento)

lista[2] = 9.8

for elemento in lista:
    print(elemento)

media = (lista[0] + lista[1] + lista[2] + lista[3]) / 4
print(f'media = {media}')

print(len(lista))

lista.append(media)
lista.extend([11,12,13,14,15])

lista.remove(15)
print(lista)

dicionario = {
    'chave1': 1,
    'chave2': 2,
}
print(dicionario)

cadastro = {
    'matricula': 438,
    'dia_cadastro': 29,
    'mes_cadastro': 9,
    'turma': '2E'
}
print(cadastro['matricula'])

cadastro['turma'] = '2G'
print(cadastro)

cadastro['modalidade'] = 'Presencial'
print(cadastro)

cadastro.pop('turma')
print(cadastro)

print(cadastro.items())
print(cadastro.keys())
print(cadastro.values())

for chaves, valores in cadastro.items():
   print(chaves, valores)

def media():
    nota_1 = float(input('Digite a primeira nota: '))
    nota_2 = float(input('Digite a segunda nota: '))

    print(f'média: {(nota_1 + nota_2) / 2}')

while True:
    print('--------◊--------')
    print('1. Calcular média')
    print('2. Sair')
    opcao = int(input('Digite uma opção: '))
    print('--------◊--------')

    if opcao == 1:
        media()
    elif opcao == 2:
        break
    else:
        print('Opção invalida...')

lista = [
    {'aluno': 'Davi', 'Nota1': 10, 'Nota2': 8},
    {'aluno': 'Leandro', 'Nota1': 7, 'Nota2': 9}
]

aux = int(input('Insira a quantidade de alunos: '))

for count in range(1, aux + 1):
    print('--------◊--------')
    print(f'Aluno {count}')
    aluno = input('Nome: ')
    nota1 = float(input('Primeira nota: '))
    nota2 = float(input('Segunda nota: '))

    lista.append({'aluno': aluno, 'Nota1': nota1, 'Nota2': nota2})

print(lista)
"""